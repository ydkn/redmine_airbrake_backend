# frozen_string_literal: true

# Create airbrake project settings
class CreateAirbrakeProjectSettings < ActiveRecord::Migration[4.2]
  def up
    create_table :airbrake_project_settings do |t|
      t.references :project, index: true, null: false
      t.references :tracker
      t.references :category
      t.references :priority
      t.string :reopen_regexp
      t.timestamps null: false
    end
  end

  def down
    drop_table :airbrake_project_settings
  end
end

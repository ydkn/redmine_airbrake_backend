ENV['RAILS_ENV'] ||= 'test'

require 'rails'
require 'rails/test_help'

Bundler.require(*Rails.groups)

$LOAD_PATH.unshift(*Rails::Engine::Configuration.new(File.expand_path('..',  __dir__)).eager_load_paths)

require_relative '../test_helper'

require 'airbrake_helper'

class AirbrakeHelperTest < ActiveSupport::TestCase
  include AirbrakeHelper

  class Repository
    attr_reader :identifier

    def initialize(options = {})
      @identifier = options[:identifier]
    end
  end

  test 'airbrake_backtrace_element_markup_without_repository' do
    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/PROJECT_ROOT/foo/bar.rb', line: 10)
    assert_equal('@[PROJECT_ROOT]/foo/bar.rb:10@', airbrake_backtrace_element_markup_without_repository(be))

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/GEM_ROOT/foo/bar.rb', line: 10)
    assert_equal('@[GEM_ROOT]/foo/bar.rb:10@', airbrake_backtrace_element_markup_without_repository(be))

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/PROJECT_ROOT/foo/bar.rb', function: 'foo')
    assert_equal('@[PROJECT_ROOT]/foo/bar.rb@', airbrake_backtrace_element_markup_without_repository(be))

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/foo/bar.rb', line: 10)
    assert_equal('@/foo/bar.rb:10@', airbrake_backtrace_element_markup_without_repository(be))

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/foo/bar.rb', function: 'foo')
    assert_equal('@/foo/bar.rb@', airbrake_backtrace_element_markup_without_repository(be))
  end

  test 'airbrake_backtrace_element_markup_with_repository' do
    r = Repository.new(identifier: nil)

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/PROJECT_ROOT/foo/bar.rb', line: 10)
    assert_equal('source:"/foo/bar.rb#L10"', airbrake_backtrace_element_markup_with_repository(r, be))

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/PROJECT_ROOT/foo/bar.rb')
    assert_equal('source:"/foo/bar.rb"', airbrake_backtrace_element_markup_with_repository(r, be))

    r = Repository.new(identifier: 'foo')

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/PROJECT_ROOT/foo/bar.rb', line: 10)
    assert_equal('source:"foo|/foo/bar.rb#L10"', airbrake_backtrace_element_markup_with_repository(r, be))

    be = RedmineAirbrakeBackend::BacktraceElement.new(file: '/PROJECT_ROOT/foo/bar.rb')
    assert_equal('source:"foo|/foo/bar.rb"', airbrake_backtrace_element_markup_with_repository(r, be))
  end
end

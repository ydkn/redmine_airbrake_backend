require_relative '../test_helper'

require 'redmine_airbrake_backend/notice'

class NoticeTest < ActiveSupport::TestCase
  test 'parse notice request' do
    params = {
      "errors" => [{
        "type" => "OpenURI::HTTPError",
        "message" => "404 Not Found",
        "backtrace" => [{
          "file" => "/usr/local/lib/ruby/2.5.0/open-uri.rb",
          "line" => 377,
          "function" => "open_http"
        }, {
          "file" => "/usr/local/lib/ruby/2.5.0/open-uri.rb",
          "line" => 755,
          "function" => "buffer_open"
        }, {
          "file" => "/GEM_ROOT/gems/activesupport-5.2.1/lib/active_support/dependencies.rb",
          "line" => 287,
          "function" => "block in require"
        }, {
          "file" => "/GEM_ROOT/gems/activesupport-5.2.1/lib/active_support/dependencies.rb",
          "line" => 253,
          "function" => "load_dependency"
        }, {
          "file" => "/GEM_ROOT/gems/activesupport-5.2.1/lib/active_support/dependencies.rb",
          "line" => 287,
          "function" => "require"
        }, {
          "file" => "bin/rails",
          "line" => 4,
          "function" => "<main>"
        }]
      }],
      "context" => {
        "rootDirectory" => "/app",
        "environment" => "production",
        "hostname" => "foobar.fail",
        "severity" => "error",
        "os" => "x86_64-linux-musl",
        "language" => "ruby/2.5.3",
        "notifier" => { "name" => "airbrake-ruby", "version" => "2.12.0" },
        "component" => "MyComponent",
        "action" => "MyAction"
      },
      "environment" => {
        "program_name" => "bin/rails"
      },
      "session" => {},
      "params" => {
        "foo" => "bar",
        "locale" => "en"
      },
      "project_id" => "1"
    }.with_indifferent_access

    notice = RedmineAirbrakeBackend::Notice.new(
      errors:      params[:errors].map { |e| RedmineAirbrakeBackend::Error.new(e) },
      params:      params[:params],
      session:     params[:session],
      context:     params[:context],
      environment: params[:environment]
    )

    assert_equal(1, notice.errors.length)
    assert_equal('OpenURI::HTTPError', notice.errors.first.type)
    assert_equal('404 Not Found', notice.errors.first.message)
    assert_equal('cfc605db663aab8c1cc7864567dab734', notice.id)
  end
end

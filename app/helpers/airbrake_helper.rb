# frozen_string_literal: true

# Helper methods for rendering airbrake errors
module AirbrakeHelper
  # Error title
  def airbrake_error_title(error)
    if error.type.blank?
      error.message
    else
      "#{error.type}: #{error.message}"
    end
  end

  # Wiki markup for a table
  def airbrake_format_table(data)
    lines = []

    data.each do |key, value|
      next if value.blank?

      if value.is_a?(String)
        lines << "|@#{key}@|@#{value}@|"
      elsif value.is_a?(Hash)
        lines << "|@#{key}@|@#{value.map { |k, v| "#{k}: #{v}" }.join(', ')}@|"
      end
    end

    lines.join("\n")
  end

  # Wiki markup for a list item
  def airbrake_format_list_item(name, value)
    return '' if value.blank?

    "* *#{name}:* #{value}"
  end

  # Wiki markup for backtrace element with link to repository if possible
  def airbrake_format_backtrace_element(repositories, element)
    repository = airbrake_repository_for_backtrace_element(repositories, element)

    markup = if repository.blank?
               airbrake_backtrace_element_markup_without_repository(element)
             else
               airbrake_backtrace_element_markup_with_repository(repository, element)
             end

    markup + " in ??<notextile>#{element.function}</notextile>??"
  end

  def airbrake_render_section(data, section)
    return nil if data.blank?

    render partial: 'airbrake/issue_description/section', locals: { data: data, section: section }
  end

  private

  def airbrake_repository_for_backtrace_element(repositories, element)
    return nil unless element.root.project?
    return nil if element.file.blank?

    (repositories || []).find { |r| r.entry(element.file) }
  end

  def airbrake_backtrace_element_markup_without_repository(element)
    file = element.root.undefined? ? element.file : "[#{element.root.upcase}_ROOT]#{element.file}"

    if element.line.blank?
      "@#{file}@"
    else
      "@#{file}:#{element.line}@"
    end
  end

  def airbrake_backtrace_element_markup_with_repository(repository, element)
    file = element.line.present? ? "#{element.file}#L#{element.line}" : element.file

    if repository.identifier.blank?
      "source:\"#{file}\""
    else
      "source:\"#{repository.identifier}|#{file}\""
    end
  end
end

# frozen_string_literal: true

require 'tempfile'

# Handling for creating / updating issues
module AirbrakeIssueHandling
  extend ActiveSupport::Concern
  include AirbrakeIssueFinding
  include AirbrakeAttachments

  private

  def reopen_issue_if_qualifies(issue, notice)
    return unless issue.persisted?
    return unless issue.status.is_closed?
    return unless reopen_issue?(notice)

    reopen_issue(issue, notice)
  end

  def create_issue!
    return if @notice.blank?
    return if @notice.errors.blank?

    @issue = find_or_initialize_issue(@notice)

    set_issue_custom_field_values(@issue, @notice)

    reopen_issue_if_qualifies(@issue, @notice)

    @issue = nil unless @issue.save
  end

  def update_occurrences(issue, custom_field_values)
    return if occurrences_field.blank?

    current_value = issue.custom_value_for(occurrences_field.id)
    current_value = current_value ? current_value.value.to_i : 0

    custom_field_values[occurrences_field.id] = (current_value + 1).to_s
  end

  def set_issue_custom_field_values(issue, notice)
    custom_field_values = {}

    # Error ID
    custom_field_values[notice_hash_field.id] = notice.id if issue.new_record?

    # Update occurrences
    update_occurrences(issue, custom_field_values)

    issue.custom_field_values = custom_field_values
  end

  def reopen_issue?(notice)
    reopen_regexp = project_setting(:reopen_regexp)

    return false if reopen_regexp.blank?
    return false if notice.environment_name.blank?

    notice.environment_name =~ /#{reopen_regexp}/i
  end

  def issue_reopen_repeat_description?
    project_setting(:reopen_repeat_description)
  end

  def reopen_issue(issue, notice)
    return if notice.environment_name.blank?

    desc  = "*Issue reopened after occurring again in _#{notice.environment_name}_ environment.*"
    desc += "\n\n#{render_description(notice, repositories: @repositories)}" if issue_reopen_repeat_description?

    issue.status = issue.tracker.default_status

    issue.init_journal(User.current, desc)

    add_attachments_to_issue(issue, notice)
  end
end

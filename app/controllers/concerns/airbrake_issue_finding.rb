# frozen_string_literal: true

# Handling for creating / updating issues
module AirbrakeIssueFinding
  extend ActiveSupport::Concern
  include AirbrakeAttachments

  private

  # Load or initialize issue by project, tracker and airbrake hash or similarity
  def find_or_initialize_issue(notice)
    find_issue_by_airbrake_id(notice) || initialize_issue(notice)
  end

  def find_issue_by_airbrake_id(notice)
    issue_ids = CustomValue
                .where(customized_type: Issue.name, custom_field_id: notice_hash_field.id, value: notice.id)
                .pluck(:customized_id)

    Issue.find_by(id: issue_ids, project_id: @project.id, tracker_id: @tracker.id)
  end

  def initialize_issue(notice)
    issue = Issue.new(
      subject:     notice.subject,
      project:     @project,
      tracker:     @tracker,
      author:      User.current,
      category:    @category,
      priority:    @priority,
      description: render_description(notice, repositories: @repositories),
      assigned_to: @assignee
    )

    add_attachments_to_issue(issue, notice)

    issue
  end
end

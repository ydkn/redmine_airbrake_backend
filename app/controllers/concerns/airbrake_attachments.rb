# frozen_string_literal: true

require 'tempfile'

# Handling for creating / updating issues
module AirbrakeAttachments
  extend ActiveSupport::Concern

  included do
    after_action :cleanup_tempfiles
  end

  private

  def add_attachment_to_issue(issue, data)
    filename = data[:filename].presence || Redmine::Utils.random_hex(16)

    file = Tempfile.new(filename)

    @tempfiles << file

    file.write(data[:data])
    file.rewind

    issue.attachments << Attachment.new(file: file, author: User.current, filename: filename)
  end

  def add_attachments_to_issue(issue, notice)
    return if notice.attachments.blank?

    @tempfiles ||= []

    notice.attachments.each do |data|
      add_attachment_to_issue(issue, data)
    end
  end

  def close_tempfile(tempfile)
    tempfile.close
  rescue StandardError
    nil
  end

  def unlink_tempfile(tempfile)
    tempfile.unlink
  rescue StandardError
    nil
  end

  def cleanup_tempfiles
    Array(@tempfiles).compact.each do |tempfile|
      close_tempfile(tempfile)
      unlink_tempfile(tempfile)
    end
  end
end

# frozen_string_literal: true

# Rendering of issue description
module AirbrakeRendering
  extend ActiveSupport::Concern

  def render_description(notice, locals)
    locals = { notice: notice, repositories: [] }.merge(locals)

    if template_exists?("airbrake/issue_description/#{notice.type}")
      render_to_string("airbrake/issue_description/#{notice.type}", layout: false, locals: locals)
    else
      render_to_string('airbrake/issue_description/default', layout: false, locals: locals)
    end
  end
end

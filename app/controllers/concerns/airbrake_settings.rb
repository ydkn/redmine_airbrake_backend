# frozen_string_literal: true

# Get Airbrake global or project settings
module AirbrakeSettings
  extend ActiveSupport::Concern

  private

  def global_setting(key)
    Setting.plugin_redmine_airbrake_backend[key.to_s]
  end

  def project_setting(key)
    return nil if @project.airbrake_settings.blank?

    @project.airbrake_settings.send(key) if @project.airbrake_settings.respond_to?(key)
  end
end

# frozen_string_literal: true

require 'redmine_airbrake_backend/notice'

# Controller with airbrake related stuff
class AirbrakeController < ::ApplicationController
  include AirbrakeSettings
  include AirbrakeIssueHandling
  include AirbrakeRendering

  skip_before_action :verify_authenticity_token
  skip_before_action :session_expiration, :user_setup, :check_if_login_required, :set_localization,
                     :check_password_change

  prepend_before_action :find_project
  prepend_before_action :check_key, :parse_key

  before_action :authorize
  before_action :find_tracker
  before_action :find_category
  before_action :find_priority
  before_action :find_assignee
  before_action :find_repositories

  private

  def find_project
    @project = Project.find(@key[:project] || params[:project_id])
  end

  def parse_key
    raw_key = if (request.headers['HTTP_AUTHORIZATION'] || '') =~ /^Bearer\s+(.*)\s*$/i # New auth method
                Regexp.last_match[1]
              elsif params[:key].present? # Old auth method
                params[:key]
              end

    @key = JSON.parse(raw_key).with_indifferent_access
  rescue StandardError
    @key = nil
  end

  def check_key
    render_error('No or invalid API key format', :unauthorized) if @key.blank? || @key[:key].blank?
  end

  def authorize
    User.current = User.find_by_api_key(@key[:key]) # rubocop:disable Rails/DynamicFindBy

    if User.current.blank? || User.current.anonymous?
      render_error('Failed to authenticate', :unauthorized)

      return
    end

    return if User.current.allowed_to?({ controller: params[:controller], action: params[:action] }, @project)

    render_error('Access Denied', :forbidden)
  end

  def find_tracker
    @tracker = record_for(@project.trackers, :tracker)

    render_error('No or invalid tracker', :failed_dependency) if @tracker.blank?

    # Check notice ID field
    return if @tracker.custom_fields.find_by(id: notice_hash_field.id).present?

    render_error('Custom field for notice hash not available on selected tracker', :failed_dependency)
  end

  def find_category
    @category = record_for(@project.issue_categories, :category)
  end

  def find_priority
    @priority = record_for(IssuePriority, :priority) || IssuePriority.default
  end

  def find_assignee
    @assignee = record_for(@project.users, :assignee, %i[id login])
  end

  def find_repositories
    @repositories = if @key[:repositories].present?
                      @project.repositories.where(identifier: @key[:repositories].split(',').map(&:strip)).to_a
                    else
                      @project.repositories.to_a
                    end
  end

  def render_error(error, status = :internal_server_error)
    ::Rails.logger.warn(error)

    render json: { error: { message: error } }, status: status
  end

  def render_airbrake_response
    if @issue.present?
      airbrake_id_value = CustomValue.find_by(
        customized_type: Issue.name,
        customized_id:   @issue.id,
        custom_field_id: notice_hash_field.id
      )

      render json: {
        id:  airbrake_id_value&.value,
        url: issue_url(@issue)
      }, status: :created
    else
      render json: {}
    end
  end

  def record_for(on, key, fields = %i[id name])
    fields.each do |field|
      val = on.find_by(field => @key[key])
      return val if val.present?
    end

    project_setting(key)
  end

  def custom_field(key)
    @project.issue_custom_fields.find_by(id: global_setting(key)) ||
      CustomField.find_by(id: global_setting(key), is_for_all: true)
  end

  def notice_hash_field
    custom_field(:hash_field)
  end

  def occurrences_field
    custom_field(:occurrences_field)
  end
end

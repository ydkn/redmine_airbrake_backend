# frozen_string_literal: true

# Project-specific settings for airbrake
class AirbrakeProjectSetting < ActiveRecord::Base # rubocop:disable Rails/ApplicationRecord
  belongs_to :project
  belongs_to :tracker
  belongs_to :category, class_name: 'IssueCategory'
  belongs_to :priority, class_name: 'IssuePriority'

  validates :project_id, presence: true
end

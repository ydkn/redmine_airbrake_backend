# frozen_string_literal: true

require 'redmine_airbrake_backend/error'

module RedmineAirbrakeBackend
  # Notice received by airbrake
  class Notice
    attr_reader :id, :subject, :type, :environment_name
    attr_reader :errors, :params, :session, :context, :environment, :application, :attachments

    def initialize(options)
      # Set instance variables from options
      instance_variables_from_options(options, %i[errors params session environment application])
      instance_variables_from_options(options, %i[attachments], ensure_array: true)

      # Context
      @context = context_from_options(options)

      # Environment name
      @environment_name = environment_name_from_options(options)

      # Type
      @type = type_from_options(options)

      # Error ID
      @id = generate_id

      # Subject
      @subject = generate_subject
    end

    private

    def instance_variables_from_options(options, keys, opts = {})
      keys.each do |key|
        value = options[key]

        value = value.compact if value.is_a?(Array) || value.is_a?(Hash)
        value = Array(value)  if opts[:ensure_array]

        instance_variable_set("@#{key}", value)
      end
    end

    def context_from_options(options)
      options[:context].reject { |k, _| ['notifier'].include?(k) }
    end

    def environment_name_from_options(options)
      return nil if options[:context].blank?

      options[:context][:environment].presence
    end

    def type_from_options(options)
      return options[:type] if options[:type].present?

      return nil if options[:context].blank? || options[:context][:language].blank?

      options[:context][:language].strip.split('/', 2).first.downcase
    end

    def generate_id
      Digest::MD5.hexdigest(@errors.map(&:id).join("\n"))
    end

    def generate_subject
      error = @errors.first

      subject = if error.type.blank? || error.message.starts_with?("#{error.type}:")
                  "[#{@id[0..7]}] #{error.message}"
                else
                  "[#{@id[0..7]}] #{error.type}: #{error.message}"
                end

      subject[0..254].strip
    end
  end
end

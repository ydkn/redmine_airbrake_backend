# frozen_string_literal: true

require 'digest/md5'

module RedmineAirbrakeBackend
  # Backtrace element received by airbrake
  class BacktraceElement
    DEFAULT_ROOT = ActiveSupport::StringInquirer.new('undefined')

    attr_reader :root, :file, :line, :function, :column

    def initialize(data)
      @root, @file = parse_root_and_file(data[:file].presence)
      @line = data[:line].presence
      @function = data[:function].presence
      @column = data[:column].presence
    end

    def checksum
      @checksum ||= Digest::MD5.hexdigest("#{root}|#{file}|#{normalized_function_name}|#{line}|#{column}")
    end

    def formatted_file
      if root.undefined?
        file
      else
        "[#{root.upcase}_ROOT]#{file}"
      end
    end

    private

    def parse_root_and_file(file)
      return DEFAULT_ROOT, file if file.blank?

      match = %r{\A/([A-Z]+)_ROOT(/.+)}.match(file.strip)

      return ActiveSupport::StringInquirer.new(match[1].downcase), match[2].strip if match.present?

      [DEFAULT_ROOT, file]
    end

    def normalized_function_name
      name = @function
             .downcase
             .gsub(/([\d_]+$)?/, '') # ruby blocks

      RedmineAirbrakeBackend.filter(name)
    end
  end
end

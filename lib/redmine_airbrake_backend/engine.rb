# frozen_string_literal: true

require 'redmine_airbrake_backend/version'

module RedmineAirbrakeBackend
  # Engine for Airbrake integration into Redmine
  class Engine < ::Rails::Engine
    initializer 'airbrake_backend.register_redmine_plugin', after: 'load_config_initializers' do |_|
      RedmineAirbrakeBackend::Engine.register_redmine_plugin
    end

    initializer 'airbrake_backend.apply_patches', after: 'airbrake_backend.register_redmine_plugin' do |_|
      RedmineAirbrakeBackend::Engine.add_redmine_patch('Project')
      RedmineAirbrakeBackend::Engine.add_redmine_patch('Tracker')
      RedmineAirbrakeBackend::Engine.add_redmine_patch('IssueCategory')
      RedmineAirbrakeBackend::Engine.add_redmine_patch('IssuePriority')
      RedmineAirbrakeBackend::Engine.add_redmine_patch('ProjectsHelper', :prepend)
    end

    def register_redmine_plugin
      Redmine::Plugin.register :redmine_airbrake_backend do
        name        'Airbrake Backend'
        author      'Florian Schwab'
        author_url  'https://ydkn.de'
        description 'Airbrake Backend for Redmine'
        url         'https://gitlab.com/ydkn/redmine_airbrake_backend'
        version     ::RedmineAirbrakeBackend::VERSION
        directory   RedmineAirbrakeBackend.directory

        requires_redmine version_or_higher: '3.0.0'

        project_module :airbrake do
          permission :airbrake, airbrake_notice: [:notices], airbrake_report: [:ios_reports]
          permission :manage_airbrake, airbrake: [:update]
        end

        settings default: { hash_field: '', occurrences_field: '' }, partial: 'settings/airbrake'
      end
    end

    def add_redmine_patch(clazz_name, mode = :include)
      require_dependency "redmine_airbrake_backend/patches/#{clazz_name.underscore}"

      clazz       = "::#{clazz_name}".constantize
      patch_clazz = "::RedmineAirbrakeBackend::Patches::#{clazz_name}".constantize

      clazz.send(mode, patch_clazz)
    end
  end
end

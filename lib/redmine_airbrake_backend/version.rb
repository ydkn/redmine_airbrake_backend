# frozen_string_literal: true

module RedmineAirbrakeBackend
  # Version of this gem
  VERSION = '1.4.1'
end

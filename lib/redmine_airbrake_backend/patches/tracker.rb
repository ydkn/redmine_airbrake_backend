# frozen_string_literal: true

require 'active_support/concern'

module RedmineAirbrakeBackend
  module Patches
    # Patches for Tracker
    module Tracker
      extend ActiveSupport::Concern

      included do
        has_many :airbrake_project_settings, dependent: :nullify
      end
    end
  end
end

# frozen_string_literal: true

require 'active_support/concern'

module RedmineAirbrakeBackend
  module Patches
    # Patches for ProjectsHelper
    module ProjectsHelper
      extend ActiveSupport::Concern

      # add airbrake tab to project settings
      def project_settings_tabs
        tabs = super

        if User.current.allowed_to?(:manage_airbrake, @project)
          tabs.push(
            name:    'airbrake',
            action:  :manage_airbrake,
            partial: 'projects/settings/airbrake',
            label:   :project_module_airbrake
          )
        end

        tabs
      end
    end
  end
end

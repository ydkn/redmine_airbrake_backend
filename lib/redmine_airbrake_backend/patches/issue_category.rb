# frozen_string_literal: true

require 'active_support/concern'

module RedmineAirbrakeBackend
  module Patches
    # Patches for IssueCategory
    module IssueCategory
      extend ActiveSupport::Concern

      included do
        has_many :airbrake_project_settings, foreign_key: :category_id, dependent: :nullify
      end
    end
  end
end

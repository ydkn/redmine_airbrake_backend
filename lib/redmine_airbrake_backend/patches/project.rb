# frozen_string_literal: true

require 'active_support/concern'

module RedmineAirbrakeBackend
  module Patches
    # Patches for Project
    module Project
      extend ActiveSupport::Concern

      included do
        has_one :airbrake_settings, class_name: 'AirbrakeProjectSetting', dependent: :destroy
      end
    end
  end
end

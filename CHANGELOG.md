# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.1] - 2019-08-26
### Fixed
- Typo in variable name
- Description syntax for source links without line number

## [1.4.0] - 2019-08-26
### Fixed
- Mapping of backtrace elements to repository entries
### Changed
- Respect new rubocop suggestions

## [1.3.0] - 2019-02-11
### Changed
- Redmine 4.0 / Rails 5.0 compatibility

## [1.2.4] - 2019-01-30
### Fixed
- Require Rake::TestTask to avoid error with Redmine >= 4.0.0

## [1.2.3] - 2019-01-27
### Fixed
- Fix reopen of issues

## [1.2.2] - 2018-11-03
### Fixed
- Fix finding repository if no filename is for backtrace element
### Changed
- Internal restructuring for better code style
- Requires Ruby >= 2.4.0
- Requires Redmine >= 3.4.0

## [1.2.1] - 2018-01-29
### Added
- All responses are formatted as JSON

## [1.2.0] - 2018-01-17
### Added
- Support airbrake token based authentication

## [1.1.2] - 2017-12-12
### Fixed
- Prevent rendering of empty sections
- Handle notices without a backtrace

## [1.1.1] - 2016-05-18
### Fixed
- Add missing foreign_key option on issue category has many relation. Caused error on removing issue category

## [1.1.0] - 2016-01-29
### Fixed
- Handle new ruby lambda backtrace syntax
- Don't treat nested errors separately

## [1.0.3] - 2016-01-26
### Fixed
- Fix syntax error

## [1.0.2] - 2016-01-26
### Fixed
- Wrong status code for response

## [1.0.1] - 2016-01-25
### Fixed
- Add space to comma separated key-value list in description tables
- Exclude notifier information from context table

## [1.0.0] - 2016-01-23
### Changed
- Add support for new v3 JSON notices
- Add support for new v3 JSON iOS reports
- Dropped support for old notices and reports APIs
- Add permission to use Airbrake API. No longer bound to issue create permission
- Added new key *column* in backtrace to ID calculation
- Require Redmine >= 3.2.0
- Show hashes in table sections (no recursion)

## [0.6.1] - 2015-04-28
### Fixed
- Usage as Redmine plugin additional to gem

## [0.6.0] - 2015-04-22
### Changed
- Redmine 3.0.x compatibility
- Switched from hpricot to nokogiri for XML parsing

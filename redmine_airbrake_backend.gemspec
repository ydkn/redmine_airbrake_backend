# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'redmine_airbrake_backend/version'

Gem::Specification.new do |spec|
  spec.required_ruby_version = '>= 2.4.0'

  spec.name          = 'redmine_airbrake_backend'
  spec.version       = RedmineAirbrakeBackend::VERSION
  spec.authors       = ['Florian Schwab']
  spec.email         = ['me@ydkn.de']
  spec.description   = 'Plugin which adds Airbrake support to Redmine'
  spec.summary       = 'This plugin provides the necessary API to use Redmine as a Airbrake backend'
  spec.homepage      = 'https://gitlab.com/ydkn/redmine_airbrake_backend'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split("\n")
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'rails'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'rubocop-rails'
end
